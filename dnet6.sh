#!/bin/bash
root=$(pwd)

dotnetver=6.0

sdkfile=/tmp/dotnetsdk.tar.gz
aspnetfile=/tmp/aspnetcore.tar.gz

download() {
    [[ $downloadspage =~ $1 ]]
    linkpage=$(wget -qO - https://dotnet.microsoft.com${BASH_REMATCH[1]})

    matchdl='id="directLink" href="([^"]*)"'
    [[ $linkpage =~ $matchdl ]]
    wget -O $2 "${BASH_REMATCH[1]}"
}

detectArch() {
  
    if command -v uname > /dev/null; then
        machineCpu=$(uname -m)-$(uname -p)

        if [[ $machineCpu == *64* ]]; then
            arch=arm64
        else
            echo "Your machine is unsupported, please run this on an ARM machine that is 64-bit."
            echo
            exit 1
        fi
    fi
}

echo "Welcome to the ARM installer for dotnet"
echo "The following versions will be installed:"
echo
echo ".NET SDK $dotnetver"
echo "ASP.NET Runtime $dotnetver"
echo

rm -f $sdkfile
rm -f $aspnetfile

dotnettype="dotnet"
dotnettype="dotnet-core"

echo "Downloading .NET SDK $dotnetver"
echo

downloadspage=$(wget -qO - https://dotnet.microsoft.com/download/$dotnettype/$dotnetver)

detectArch

download 'href="([^"]*sdk-[^"/]*linux-'$arch'-binaries)"' $sdkfile

echo "Downloading ASP.NET Runtime $dotnetver"
echo

download 'href="([^"]*aspnetcore-[^"/]*linux-'$arch'-binaries)"' $aspnetfile

if [[ -d /opt/dotnet ]]; then
    echo "/opt/dotnet already  exists on your filesystem."
else
    echo "Creating Main Directory"
    echo ""
    sudo mkdir /opt/dotnet
fi

echo "Installing .NET SDK $dotnetver"
echo
sudo tar -xvf $sdkfile -C /opt/dotnet/

echo "Installing ASP.NET Runtime $dotnetver"
echo
sudo tar -xvf $aspnetfile -C /opt/dotnet/

sudo ln -s /opt/dotnet/dotnet /usr/local/bin

if grep -q 'export DOTNET_ROOT=' /home/pi/.bashrc;  then
  echo 'Already added link to .bashrc'
else
  echo 'Adding Link to .bashrc'
  echo 'export DOTNET_ROOT=/opt/dotnet' >> /home/pi/.bashrc
fi

clear

dotnet --info

echo
echo "Install finished."
