#!/bin/bash

echo "Welcome to the .NET installer for ARM"
echo
sleep 3s

root=$(pwd)
choice=6

base_url="https://gitlab.com/hokutochen/dotnet-arm/-/raw/main"

dnet6="dnet6.sh"
dnet5="dnet5.sh"
dnet3_1="dnet3-1.sh"

while [ $choice -eq 6 ]; do

        echo "The default version to install is .NET 6"
        echo
        echo "Please choose the folllowing options"
        echo "1. Continue"
        echo "2. Install .NET 5"
        echo "3. Install .NET 3.1 (untested)"
		echo "4. Exit"
        echo -n "Type in the number of an option and press ENTER"
        echo
        read choice

        if [[ $choice -eq 1 ]] ; then
		        echo
		        echo "Downloading .NET 6"
	    	    rm "$root/$dnet6" 1>/dev/null 2>&1
    		    wget -N "$base_url/$dnet6" && bash "$root/$dnet6"
	    	    echo
	        	choice=6
        elif [[ $choice -eq 2 ]] ; then
	    	    echo
	    	    echo "Downloading .NET 5"
	    	    rm "$root/$dnet5" 1>/dev/null 2>&1
    		    wget -N "$base_url/$dnet5" && bash "$root/$dnet5"
	    	    echo
	    	    sleep 2s
	    	    choice=6
	    elif [[ $choice -eq 3 ]] ; then
	    	    echo
	    	    echo "Downloading .NET 3.1"
	    	    rm "$root/$dnet3_1" 1>/dev/null 2>&1
    		    wget -N "$base_url/$dnet3_1" && bash "$root/$dnet3_1"
	    	    echo
	    	    sleep 2s
	    	    choice=6
		elif [[ $choice -eq 4 ]] ; then
				echo ""
				echo "Exiting..."
				cd "$root"
				exit 0
        else
	    	    echo "Invalid choice"
	    	    echo ""
		        choice=6
        fi
done

cd "$root"
exit 0
