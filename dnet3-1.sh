#!/bin/bash -e
root=$(pwd)
echo ""

function detect_OS_ARCH_VER_BITS {
    ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
    if [ -f /etc/lsb-release ]; then
        . /etc/lsb-release
        if [ "$DISTRIB_ID" = "" ]; then
            OS=$(uname -s)
            VER=$(uname -r)
        else
            OS=$DISTRIB_ID
            VER=$DISTRIB_RELEASE
        fi
    elif [ -f /etc/debian_version ]; then
        OS=Debian  # XXX or Ubuntu??
        VER=$(cat /etc/debian_version)
        SVER=$( grep < /etc/debian_version -oP "[0-9]+" | head -1 )
    elif [ -f /etc/centos-release ]; then
        OS=CentOS
        VER=$( grep < /etc/centos-release -oP "[0-9]+" | head -1 )
    elif [ -f /etc/fedora-release ]; then
        OS=Fedora
        VER=$( grep < /etc/fedora-release -oP "[0-9]+" | head -1 )
    elif [ -f /etc/os-release ]; then
        . /etc/os-release
        if [ "$NAME" = "" ]; then
          OS=$(uname -s)
          VER=$(uname -r)
        else
          OS=$NAME
          VER=$VERSION_ID
        fi
    else
        OS=$(uname -s)
        VER=$(uname -r)
    fi
    case $(uname -m) in
    x86_64)
        BITS=64
        ;;
    i*86)
        BITS=32
        ;;
    armv*)
        BITS=32
        ;;
    *)
        BITS=?
        ;;
    esac
    case $(uname -m) in
    x86_64)
        ARCH=x64  # or AMD64 or Intel64 or whatever
        ;;
    i*86)
        ARCH=x86  # or IA32 or Intel32 or whatever
        ;;
    *)
        # leave ARCH as-is
        ;;
    esac
}

declare OS ARCH VER BITS

detect_OS_ARCH_VER_BITS

export OS ARCH VER BITS

if [ "$BITS" = 32 ]; then
    echo -e "Your system architecture is $ARCH which is unsupported to run Microsoft .NET Core SDK. \nYour OS: $OS \nOS Version: $VER"
    echo
    rm n-prereq.sh
    exit 1
fi

if [ "$OS" = "Ubuntu" ]; then
    supported_ver=("16.04" "18.04" "20.04" "21.04" "21.10")

    if [[ " ${supported_ver[@]} " =~ " ${VER} " ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$OS" = "LinuxMint" ]; then
    SVER=$( echo $VER | grep -oP "[0-9]+" | head -1 )
    supported_ver=("19" "20")

    if [[ " ${supported_ver[@]} " =~ " ${SVER} " ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$supported" = 0 ]; then
    echo -e "Your OS $OS $VER $ARCH looks unsupported to run Microsoft .NET Core. \nExiting..."
    rm n-prereq.sh
    exit 1
fi

if [ "$OS" = "Linux" ]; then
    echo -e "Your OS $OS $VER $ARCH probably can run Microsoft .NET Core."
    rm n-prereq.sh
    exit 1
fi

echo ""

if [ "$OS" = "Ubuntu" ]; then
    echo ""
    echo "Preparing to install..."
    
    sudo apt update
    sudo apt upgrade

    echo "Installing dotnet"
    echo "This installation will install the following dotnet versions"
    echo "dotnet sdk - 3.1.417"
    echo "dotnet aspnetcore runtime - 3.1.23"
     
    cd /usr/share
    sudo mkdir dotnet-arm64
    cd /usr/share/dotnet-arm64

    sudo wget https://download.visualstudio.microsoft.com/download/pr/6aaec6b7-3d0c-4f00-a8a0-a53c4844525b/e932447b5c2e854bcae25942a2113af2/dotnet-sdk-3.1.419-linux-arm64.tar.gz

    sudo wget https://download.visualstudio.microsoft.com/download/pr/b48947af-04a3-400f-aa2b-08e5079fd95b/a0d2bd903730864cfa3254a20658aa33/aspnetcore-runtime-3.1.25-linux-arm64.tar.gz

    sudo tar zxf dotnet-sdk-3.1.418-linux-arm64.tar.gz
    sudo tar zxf aspnetcore-runtime-3.1.24-linux-arm64.tar.gz

    echo export DOTNET_ROOT=/usr/share/dotnet-arm64
    echo export PATH=$PATH:/usr/share/dotnet-arm64
    sudo rm dotnet-sdk-3.1.418-linux-arm64.tar.gz aspnetcore-runtime-3.1.24-linux-arm64.tar.gz

    if grep -q 'export DOTNET_ROOT=' .bashrc;  then
        echo 'Already added link to .bashrc'
    else
        echo 'Adding Link to .bashrc'
        echo 'export DOTNET_ROOT=/opt/dotnet' >> .bashrc
    fi
fi

echo
read -n 1 -s -p "Press any key to continue..."
sleep 2

cd "$root"
rm "$root/dnet6.sh"
exit 0
